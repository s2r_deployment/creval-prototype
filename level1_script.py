import sys
import numpy as np
import pandas as pd
import geopy.distance
import mysql.connector as connection

def query_sql_to_pandas(con_data, query):
    try:
        mydb = connection.connect(
            host=con_data[0],
            database = con_data[1],
            user=con_data[2],
            passwd=con_data[3],
            port=con_data[4]
            ) #use_pure=True

        result_dataFrame = pd.read_sql(query,mydb)
        mydb.close()
        return result_dataFrame
    except Exception as e:
        mydb.close()
        print(str(e))

    return None


def query_insert_rows(con_data, query):
    mydb = connection.connect(
        host=con_data[0],
        database=con_data[1],
        user=con_data[2],
        passwd=con_data[3],
        port=con_data[4]
    )

    cursor = mydb.cursor()

    # Insert new row
    cursor.execute(
        query)

    # Make sure data is committed to the database
    mydb.commit()

    cursor.close()
    mydb.close()


def comps_conditions(df_risk, df_input, app_id, params):

   # def compute_distance(row, coords_1):
     #   coords_2 = (row.latitude, row.longitude)
     #   return geopy.distance.distance(coords_1, coords_2).miles

    time_span = params["time_span"]
    n_units = params["n_units"]
    year_built = params["year_built"]
    gross_liv_area = params["gross_liv_area"]
    tot_bedr = params["tot_bedr"]
    park_per_bedr = params["park_per_bedr"]
    levels = params["levels"]
    radius = params["radius"]
    price_per_unit_weight = params["price_per_unit_weight"]
    price_per_gim_weight = params["price_per_gim_weight"]
    price_per_bedr_weight = params["price_per_bedr_weight"]
    cap_rate_weight = params["cap_rate_weight"]

    # Input data
    df_input = query_sql_to_pandas(con_data=con_data,
                                   query= f"SELECT * FROM db_a37b5d_captu.apprequests WHERE id = {app_id};")


    #Historical data
    df_risk = df_risk[~df_risk.propertyAddress.isna()]
    #df_risk = df_risk[df_risk.saleCond == 'CashEquiv'] # Not needed here - For next Levels

    df_risk['GIM'] = np.where(df_risk['incGross'] == 0, np.nan, df_risk['priceSale'] / df_risk['incGross'])
    df_risk['totalBr'] = df_risk.oneBr*1 + df_risk.twoBr*2 + df_risk.threeBr*3
    df_risk['parkingSp_per_Br'] = np.where(df_risk['totalBr'] == 0, np.nan, df_risk.parkingSp/df_risk['totalBr'])


    # Input data
    df = df_input

    df['totalBr'] = df.Num_bedrooms
    df['units'] = df.NumUnits
    df['parkingSp'] = df.TotalParkingSpaces
    df['parkingSp_per_Br'] = np.where(df['totalBr'] == 0, np.nan, df.parkingSp / df['totalBr'])


    # Radius calculation
    
    coords_1 = (df.Latitude.iloc[0], df.Longitude.iloc[0])
    #df_risk['radius'].apply(lambda x : compute_distance(x, coords_1), axis=1)
    
    
    df_risk['radius'] = None

    for i in range(len(df_risk)): 
        coords_2 = (df_risk.latitude.iloc[i], df_risk.longitude.iloc[i])
        df_risk['radius'].values[i] = geopy.distance.distance(coords_1, coords_2).miles


    # Conditions
                            #Property type match 
    df_comps = df_risk[ (      #(df_risk.propertyType == df.propertyType.iloc[0]) & # Not needed here - for next levels
                       
                         #Radius: 
                   (df_risk.radius <= radius) &
                         #Time span is 5 years back
                  (df_risk.dateSale >= df.SaleDate.iloc[0] - pd.offsets.DateOffset(years=time_span)) & (df_risk.dateSale < df.SaleDate.iloc[0]) &
                         #Number of units:  
                  (df_risk.units >= df.units.iloc[0]*(1-n_units[0])) & (df_risk.units <= df.units.iloc[0]*(1+n_units[1]))  &
                         #Year built:  
                    (df_risk.yrBlt >= df.YearBuilt.iloc[0] - year_built[0]) & (df_risk.yrBlt <= df.YearBuilt.iloc[0] + year_built[1]) &
                         #Gross living area:  
                    (df_risk.sfBldg >= df.BuildingSqFt.iloc[0]*(1-gross_liv_area[0])) & (df_risk.sfBldg < df.BuildingSqFt.iloc[0]*(1+gross_liv_area[0])) &
                         #Total bedrooms:  
                    (df_risk.totalBr >= df.totalBr.iloc[0]*(1-tot_bedr[0])) & (df_risk.totalBr < df.totalBr.iloc[0]*(1+tot_bedr[1])) &
                         #Parking per bedroom ratio: 
                    (df_risk.parkingSp_per_Br >= df.parkingSp_per_Br.iloc[0]*(1-park_per_bedr[0])) & (df_risk.parkingSp_per_Br < df.parkingSp_per_Br.iloc[0]*(1+park_per_bedr[0])) &

                  #Levels
                  ( ((df_risk.levels == levels[0][0]) & (df.NumFloors.iloc[0] == levels[0][1])) |
          ((df_risk.levels == levels[1][0]) & (df.NumFloors.iloc[0] == levels[1][1])) |
          ((df_risk.levels >= levels[2][0][0]) & (df_risk.levels <= levels[2][0][1]) & (df.NumFloors.iloc[0] == levels[2][1])) |
          ((df_risk.levels >= levels[3][0][0]) & (df_risk.levels <= levels[3][0][1]) & (df.NumFloors.iloc[0] == levels[3][1])) |
          ((df_risk.levels >= levels[4][0][0]) & (df_risk.levels <= levels[4][0][1]) & (df.NumFloors.iloc[0] == levels[4][1])) |
          ((df_risk.levels >= levels[5][0][0]) & (df_risk.levels <= levels[5][0][1]) & (df.NumFloors.iloc[0] >= levels[5][1][0]) & (df.NumFloors.iloc[0] <= levels[5][1][1]) ) |
          ( (df_risk.levels >= df.levels.iloc[0]*(1-levels[6][0][0])) & (df_risk.levels <= df.NumFloors.iloc[0]*(1+levels[6][0][1])) & (df.NumFloors.iloc[0] >= levels[6][1]) )

            ) )]

    # Reliability score
    if len(df_comps) <= 3:
        reliability_score = 1
        recommended_level = 'Appraisal full'
    elif len(df_comps) <= 5:
        reliability_score = 2
        recommended_level = 'Appraisal full'
    elif len(df_comps) <= 8:
        reliability_score = 3
        recommended_level = 'Appraisal limited'
    elif len(df_comps) <= 12:
        reliability_score = 4
        recommended_level = 'Appraisal or evaluation'
    elif len(df_comps) <= 20:
        reliability_score = 5
        recommended_level = 'Evaluation or Level I'
    elif len(df_comps) <= 40:
        reliability_score = 6
        recommended_level = 'CR Level I return value'
    else:
        reliability_score = 7
        recommended_level = 'CR Level I return value'

    # Predictions
    price_per_units_median_weight = np.nanmedian(df_comps['priceSale']) / df['units'] * price_per_unit_weight
    price_per_GIM_median_weight = np.nanmedian(df_comps['GIM']) * df['incGross'] * price_per_gim_weight
    price_per_bed_median_weight = (np.nanmedian(df_comps['priceSale']) / df['totalBr']) * price_per_bedr_weight
    cap_rate_median_weight = (df['noi'] / np.nanmedian(df_comps['cap'])) * cap_rate_weight
    prediction_total = np.nansum([price_per_units_median_weight, price_per_GIM_median_weight, price_per_bed_median_weight, cap_rate_median_weight])

    return prediction_total, reliability_score, recommended_level, df['Id'].iloc[0], df['ValuationId'].iloc[0], df['SaleDate']



def main():
    con_data = ["mysql6013.site4now.net", "db_a37b5d_captu", "a37b5d_captu", "M@rra123", "3306"]

    df_property_data = query_sql_to_pandas(con_data=con_data, query="SELECT * FROM db_a37b5d_captu.propertydata;")

    # parameters
    params = {
                "time_span" : 5,  # For how much time back from Sales' date to search
                "n_units" : [0.3,0.4],  # Number of units:  x% down, y% up from subject number of units
                "year_built" : [20,20],  # Year built: x years down, y years up from subject year built
                "gross_liv_area" : [0.5,1.0],  # Gross living area:  x% down, y% up from subject gross living area
                "tot_bedr" : [0.5,1.0],  # Total bedrooms (calculated field):  x% down, y% up from subject total bedrooms
                "park_per_bedr" : [0.3,0.4],  # Parking per bedroom ratio (calculated field):  x% down, y% up from subject ratio
                "levels" : [[1,1],[2,2],[[2,4],3],[[3,5],4],[[4,6],5],[[5,12],[6,10]],[[0.5,0.7],11]],  # Meet the criteria if match between subject and search levels criterias
                "radius" : 2,  # Max Distance (in miles) to search from subject location (based on latitude and longitude)
                "price_per_unit_weight" : 0.35,  # From 0 to 1
                "price_per_gim_weight" : 0.25,  # From 0 to 1
                "price_per_bedr_weight" : 0.2,  # From 0 to 1
                "cap_rate_weight" : 0.2,  # From 0 to 1
            }
   
    
    assert np.isclose(params["price_per_unit_weight"]+params["price_per_gim_weight"]+params["price_per_bedr_weight"]+params["cap_rate_weight"], 1)

    # Apply the rules given the parameters
    valuationAmount, reliability_score, recommendationText, id, valuationid, saleDate = comps_conditions(df_property_data, df_input, app_id, params)
    
    query = f"INSERT INTO db_a37b5d_captu.appresponses (AppraisalRequestId, ValuationId, ValuationAmount, reliabilityScore, recommendationText, ValuationDate) VALUES ({id}, {valuationid}, {valuationAmount}, {reliability_score}, {recommendationText}, {saleDate});"
    
    # Insert the prediction result into the Output table
    query_insert_rows(con_data, query)

if __name__ == "__main__":
    main()
    sys.exit(0)