﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Appraiser.Data.Entities
{
    public class PropertyData
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(45)]
        public string propertyAddress { get; set; }

        [MaxLength(45)]
        public string propertyType { get; set; }

        [MaxLength(45)]
        public string saleCond { get; set; }

        public DateTime dateSale { get; set; }

        public decimal priceSale { get; set; }

        public int sfLand { get; set; }

        public int sfBldg { get; set; }

        public int yrBlt { get; set; }

        public int parkingSp { get; set; }

        public int units { get; set; }

        [MaxLength(10)]
        public string bldgClass { get; set; }

        [MaxLength(45)]
        public string starRating { get; set; }

        public decimal grossRentMultiplier { get; set; }

        public decimal noi { get; set; }

        public decimal expTot { get; set; }

        public decimal cap { get; set; }

        [MaxLength(45)]
        public string cond { get; set; }

        public int zeroBr { get; set; }
        public int oneBr { get; set; }
        public int twoBr { get; set; }
        public int threeBr { get; set; }
        public int levels { get; set; }
        public decimal incGross { get; set; }
        public decimal priceasking { get; set; }
        public decimal downPayment { get; set; }
        public decimal latitude { get; set; }
        public decimal longitude { get; set; }
        public string salePriceComment {get;set;}
    }
}
