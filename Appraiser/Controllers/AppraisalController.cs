﻿using Appraiser.Data;
using Appraiser.Data.Models.Request;
using Appraiser.Data.Models.Response;
using Appraiser.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Appraiser.Controllers
{
    [Authorize]
    [ApiController]
  //  [Route("api/[controller]")]
    public class AppraisalController : Controller
    {
        private IAppraiserService _appraiserService;

        public AppraisalController(IAppraiserService appraiserService )
        {
            _appraiserService = appraiserService;
        }

        // POST: AppraisalController/requestAppraisal
        [HttpPost]
        [Route("api/requestappraisal")]
        [ProducesResponseType(typeof(AppraisalResponseDTO), StatusCodes.Status200OK)]
        //[ProducesResponseType(typeof(Nullable), StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(typeof(Nullable), StatusCodes.Status400BadRequest)]        
        public ActionResult requestAppraisal(AppraisalRequestDTO request)
        {
            if (ModelState.IsValid)
                return Ok(_appraiserService.RequestAppraisal(request));
            else
            {
                return BadRequest(ModelState);
            }
               
        }
        
    }
}
